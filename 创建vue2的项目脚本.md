## 创建vue2的项目脚本

~~~~vue
//执行创建命令
vue create 项目名称
//进行选择手动选择 ——————

下载等待
//讲项目放到编辑器中启动下载依赖
cd 项目名
进入项目之后下载axios,element-ui,vuex,vuex-persist
cnpm i axios element-ui vuex vuex-persist node-sass@4.14.1 sass-loader@7.3.1 --save
//进行配置文件
~~~~

### 配置axios

~~~~js
创建一个utils文件夹将request.js放入
//进入request.js中
//1.引入axios文件
import axios from "axios"
//2.进行axios的创建实例化对象
const Server = axios.create({
    baseURL:"" ,//基地址的配置
    timeout:5000,//请求发送的时间
})
//3. 配置请求拦截器，前置拦截器
Server.interceptors.request.use((config)=>{
    //必须返回配置信息，配置token值
    console.log("请求拦截器触发");
    
    return config;
}, (error)=>{
    //请求发送时候，报错的时候触发的
    return Promise.reject(error);
})


//4. 配置后置拦截器，或者叫相应拦截器
Server.interceptors.response.use((res)=>{
    //返回数据，过滤器服务端返回数据
    console.log("我是后置拦截器触发");
    //把数据给他过滤掉，取出axios的data
    return res.data;
}, (error)=>{
    //请求发送时候，报错的时候触发的
    return Promise.reject(error);
})
//5.抛出对象
export default Server

~~~~

### 在main.js中配置axios

~~~~js
//引入axios
import axios from "./utils/request"
//挂在到原型上去
Vue.prototype.$axios = axios
~~~~

### 配置element-ui

~~~js
//直接在main.js中配置
//配置element-ui
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);
~~~

### 配置vuex以及vuex数据的持久化插件

~~~~vue
//创建一个store.js文件
//引入vue
import Vue from "vue"
//引入vuex
import Vuex from "vuex"
//使用vuex
Vue.use(Vuex)
//配置数据的持久化
//引入vuex-persist插件
import VuexPersist from "vuex-persist"
//进行创建存储方式
const VuexLocal = new VuexPersist({
    storage:window.localStorage
})
//进行抛出
export default new Vuex.Store({
    state:{

    },
    mutations:{

    },
    getters:{

    },
    //配置持久化
    plugins:[VuexLocal.plugin]
})
~~~~

### 在main.js中引入vuex的插件

~~~~js
import Vue from 'vue'
import App from './App.vue'
import router from './router'
//引入axios
import axios from "./utils/request"
//挂在到原型上去
Vue.prototype.$axios = axios
//配置element-ui
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);
//进行配置Vuex
import store from "./store"

Vue.config.productionTip = false

new Vue({
  router,
  store,//将vuex注入vue的实例当中去
  render: h => h(App)
}).$mount('#app')
~~~~

### 之后进行npm run serve启动项目